from src.Parse.parser import parseOBJ
from src.Generate.model import genModel, ModelConfig
from src.config import *

import os
import argparse
import json


def parseConfig(configFile):
    with open(configFile, 'r', encoding='utf-8') as f:
        return json.load(f)


def parseCullface(cullface):
    if not cullface:
        return []
    if "all" in cullface:
        return [ True, True, True, True, True, True ]

    c = [False, False, False, False, False, False]
    if "u" in cullface:
        c[0] = True
    if "d" in cullface:
        c[1] = True
    if "n" in cullface:
        c[2] = True
    if "s" in cullface:
        c[3] = True
    if "w" in cullface:
        c[4] = True
    if "e" in cullface:
        c[5] = True
    return c


def createConfig(args):

    # Name files correctly
    objName = args.name[0]
    mtlName = args.mtl
    if not args.mtl:
        mtlName = objName.rstrip('obj') + 'mtl'
    if not args.out:
        outName = os.path.basename(objName).rstrip('.obj')
    else:
        outName = args.out.rstrip('.json')

    # Make config
    mc = ModelConfig(objName, mtlName, outName,
                     parent=args.parent,
                     globalShade=args.globalShade,
                     ambientOcclusion=args.ambientOcclusion,
                     compactJson=args.compactJson,
                     particle=args.particle)
    mc.setCullface(*parseCullface(args.cullfaces))
    return mc


def printOverview(objName, mtlName, obj, mc):
    st = f"""\
[FILE OVERVIEW]

Name: {objName}
MTL : {mtlName}

Number of cubes: {len(obj.objs)}
"""

    for ii, cube in enumerate(obj.objs):
        st += f"""\
        
- Cube n#{ii} - "{cube.name}":
    nb Faces: {len(cube.f)}
    nb Vertices: {len(cube.v)}
    nb Texture Vertices: {len(cube.vt)}
    list Used Textures:
"""
        vts = []
        for ii, face in enumerate(cube.f):
            if cube.fMtl[ii] in vts:
                continue
            vts.append(cube.fMtl[ii])
            st += f"""\
        - {DEFAULT_BASEPATH + '/' + obj.mtls[cube.fMtl[ii]].name}
"""
    print(st)



if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Convert a wavefront OBJ file to the Minecraft JSON format.')
    parser.add_argument('name', metavar='O', type=str, nargs=1,
                        help='Wavefront OBJ file name')

    parser.add_argument('--mtl', '-m', dest='mtl', action='store',
                        help='Specifies the associated MTL file.')

    parser.add_argument('--parent', '-p', dest='parent', action='store',
                        default=DEFAULT_PARENT,
                        help='Specifies the parent model (default : "' + DEFAULT_PARENT + '"')

    parser.add_argument('--overview', '-o', dest='overview', action='store_const',
                        const=True, default=False,
                        help='Only show information on the input file. Useful to check its conformity.')

    parser.add_argument('--globalShade', '-s', dest='globalShade', action='store_const',
                        const=False, default=None,
                        help='Disable global shade of the object.')

    parser.add_argument('--ambientOcclusion', '-a', dest='ambientOcclusion', action='store_const',
                        const=False, default=None,
                        help='Disable ambient occulsion of the object.')

    parser.add_argument('--cullfaces', '-c', dest='cullfaces', action='store',
                        help='Activate cullface for specified face(s) ( all, w, e, s, n, u, d)')

    parser.add_argument('--compactJson', dest='compactJson', action='store_const',
                        const=True, default=False,
                        help='Make output JSON compact.')

    parser.add_argument('--out', dest='out', action='store',
                        default=None,
                        help='Specify output file name.')

    parser.add_argument('--particle', '-t', dest='particle', action='store',
                        default=None,
                        help='Specify block break particles texture.')

    # parse args
    args = parser.parse_args()

    mc = createConfig(args)

    # parse OBJ and MTL
    obj = parseOBJ(mc.objName, mc.mtlName)

    # Gen or display
    if args.overview:
        printOverview(mc.objName, mc.mtlName, obj, mc)
    else:
        genModel(mc.outName + '.json', obj, mc)


