
# North = -Z / South = +Z
# Up = +Y / Down = -Y
# East = +X / West = -X

import json
from src.config import *

class ModelConfig:

    class DisplayPosition:
        thirdperson_righthand = "thirdperson_righthand"
        thirdperson_lefthand = "thirdperson_lefthand"
        firstperson_righthand = "firstperson_righthand"
        firstperson_lefthand = "firstperson_lefthand"
        gui = "gui"
        head = "head"
        ground = "ground"
        fixed = "fixed"

    def __init__(self,
                 objName = None,
                 mtlName = None,
                 outName = None,
                 compactJson = None,
                 parent = None,
                 ambientOcclusion = None,
                 textures = None,
                 globalShade = None,
                 rotation = None,
                 display = None,
                 cullface = None,
                 particle = None):
        self.objName = objName
        self.mtlName = mtlName
        self.outName = outName
        self.compactJson = compactJson
        self.parent = parent
        self.ambientOcclusion = ambientOcclusion
        self.textures = textures
        self.globalShade = globalShade
        self.rotation = rotation
        self.display = display
        self.cullface = cullface
        self.particle = particle

    def setRotation(self, origin = [8, 8, 8], axis = "y", angle = 0, rescale = False):
        self.rotation = {
            "origin" : origin,
            "axis" : axis,
            "angle" : angle,
            "rescale" : rescale
        }

    def addDisplay(self, position, rotation = [0, 0, 0], translation = [0, 0, 0], scale = [1, 1, 1]):
        self.display[position] = {
            "rotation" : rotation,
            "translation" : translation,
            "scale" : scale
        }

    def setCullface(self, u = False, d = False, n = False, s = False, w = False, e = False):
        cullface = {
            "north": n,
            "south": s,
            "east": e,
            "west": w,
            "up": u,
            "down": d,
        }
        self.cullface = cullface




def addElement(model, obj, cube, modelConfig):

    element = {}

    if modelConfig.rotation is not None:
        element["rotation"] = modelConfig.rotation
    if modelConfig.globalShade is not None:
        element["shade"] = modelConfig.globalShade

    element["from"] = [cube.xMin, cube.yMin, cube.zMin]
    element["to"] = [cube.xMax, cube.yMax, cube.zMax]

    faces = {}

    for ii, face in enumerate(cube.f):
        if cube.hasVts():
            vts = [ cube.vt[c[1]] for c in face ]
            (x1, y1), (x2, y2) = cube.getSquare(vts)
        key = "unknown"
        if ii == cube.north:
            key = "north"
        elif ii == cube.south:
            key = "south"
        elif ii == cube.east:
            key = "east"
        elif ii == cube.west:
            key = "west"
        elif ii == cube.up:
            key = "up"
        elif ii == cube.down:
            key = "down"

        faces[key] = {}
        if cube.hasVts():
            faces[key]["uv"] = [x1, y1, x2, y2]
        if cube.fMtl[ii] is not None:
            faces[key]["texture"] = obj.mtls[cube.fMtl[ii]].name
        if modelConfig.cullface[key]:
            faces[key]["cullface"] = key

    element["faces"] = faces

    model["elements"].append(element)


def addTextures(model, obj, modelConfig):

    if len(obj.mtls) == 0:
        return

    prefix = DEFAULT_BASEPATH

    textures = {}

    for m in obj.mtls:
        # check if prefix already given
        if len(m.name.split("/")) > 1:
            textures[m.name] = m.name
        else:
            textures[m.name] = prefix + '/' + m.name

    if modelConfig.particle is not None:
        if len(modelConfig.particle.split("/")) > 1:
            textures["particle"] = modelConfig.particle
        else:
            textures["particle"] = prefix + '/' + modelConfig.particle
    else:
        if len(obj.mtls) > 0:
            if len(m.name.split("/")) > 1:
                textures["particle"] = obj.mtls[0].name
            else:
                textures["particle"] = prefix + '/' + obj.mtls[0].name


    model["textures"] = textures


def genModel(filename, obj, modelConfig):

    model = {
        "elements": [],
    }

    addTextures(model, obj, modelConfig)

    if modelConfig.parent is not None:
        if len(modelConfig.parent.split("/")) > 1:
            model["parent"] = modelConfig.parent
        else:
            model["parent"] = DEFAULT_BASEPATH + '/' + modelConfig.parent

    if modelConfig.ambientOcclusion is not None:
        model["ambientOcclusion"] = modelConfig.ambientOcclusion
    # if modelConfig.textures != None:
    #     model["textures"] = modelConfig.textures
    if modelConfig.display is not None:
        model["display"] = modelConfig.display

    for cube in obj.objs:
        addElement(model, obj, cube, modelConfig)

    if modelConfig.compactJson or DEFAULT_COMPACT_JSON:
        str = json.dumps(model)
    else:
        str = json.dumps(model, indent=2)

    with open(filename, 'w') as f:
        f.write(str)
        f.close()

