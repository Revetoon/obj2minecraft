from src.config import *


TEXTURE_SIZE = 16 # modify according to texture quality


class Obj:
    objs = []
    mtls = []

    def __init__(self):
        self.objs = []

    def addCube(self, name = ""):
        c = self.lastCube()
        vOffset = 0
        vtOffset = 0
        if c:
            vOffset = c.vOffset + len(c.v)
            vtOffset = c.vtOffset + len(c.vt)
        self.objs.append(Cube(name, vOffset, vtOffset))

    def addMtl(self, name):
        self.mtls.append(Material(name))

    def lastCube(self):
        if len(self.objs) == 0:
            return None
        return self.objs[-1]

    def lastMtl(self):
        if len(self.mtls) == 0:
            return None
        return self.mtls[-1]

    def getMtlIdx(self, name):
        for ii, m in enumerate(self.mtls):
            if m.name == name:
                return ii
        return None

    def validate(self):
        return all(c.validate() for c in self.objs)


class Cube:

    def __init__(self, name = "", vOffset = 0, vtOffset = 0):
        self.v = []
        self.f = []
        self.vt = []
        self.fMtl = [] # material index for each face

        self.xLen = 0
        self.yLen = 0
        self.zLen = 0

        self.xMin = MC_CUBE_BOUND_MAX
        self.xMax = MC_CUBE_BOUND_MIN
        self.yMin = MC_CUBE_BOUND_MAX
        self.yMax = MC_CUBE_BOUND_MIN
        self.zMin = MC_CUBE_BOUND_MAX
        self.zMax = MC_CUBE_BOUND_MIN

        self.north = None
        self.south = None
        self.east = None
        self.west = None
        self.up = None
        self.down = None

        self.vOffset = vOffset
        self.vtOffset = vtOffset

        self.currentMtlIdx = None
        self.name = name

    def addV(self, vert):
        x, y, z = vert
        vert = [round(((x + OBJ_CUBE_OFFSET) / OBJ_CUBE_UNIT) * MC_CUBE_UNIT),
                round(((y + OBJ_CUBE_OFFSET) / OBJ_CUBE_UNIT) * MC_CUBE_UNIT),
                round(((z + OBJ_CUBE_OFFSET) / OBJ_CUBE_UNIT) * MC_CUBE_UNIT)]
        self.v.append(vert)
        self.__updateLen(vert)

    def addVT(self, vt):
        x, y = vt
        vt = [round(x * TEXTURE_SIZE),
              TEXTURE_SIZE - round(y * TEXTURE_SIZE)]
        self.vt.append(vt)


    def addF(self, face):
        (v1,vt1), (v2,vt2), (v3,vt3), (v4,vt4) = face
        face = [[v1 - self.vOffset - 1, vt1 - self.vtOffset - 1],
                [v2 - self.vOffset - 1, vt2 - self.vtOffset - 1],
                [v3 - self.vOffset - 1, vt3 - self.vtOffset - 1],
                [v4 - self.vOffset - 1, vt4 - self.vtOffset - 1]]
        self.f.append(face)
        self.fMtl.append(self.currentMtlIdx)


    def hasVts(self):
        return len(self.vt) > 0

    def __updateLen(self, newV):
        x, y, z = newV
        if x < self.xMin:
            self.xMin = x
        elif x > self.xMax:
            self.xMax = x

        if y < self.yMin:
            self.yMin = y
        elif y > self.yMax:
            self.yMax = y

        if z < self.zMin:
            self.zMin = z
        elif z > self.zMax:
            self.zMax = z

        if not self.__checkBounds():
            raise Exception("Object out of bound. Must be in [ " + str(MC_CUBE_BOUND_MIN) + "; " + str(MC_CUBE_BOUND_MAX) + " ].")

        self.xLen = round(self.xMax - self.xMin, 2)
        self.yLen = round(self.yMax - self.yMin, 2)
        self.zLen = round(self.zMax - self.zMin, 2)


    def __checkBounds(self):
        if self.xMin < MC_CUBE_BOUND_MIN or self.xMax > MC_CUBE_BOUND_MAX or \
           self.yMin < MC_CUBE_BOUND_MIN or self.yMax > MC_CUBE_BOUND_MAX or \
           self.zMin < MC_CUBE_BOUND_MIN or self.zMax > MC_CUBE_BOUND_MAX:
            return False
        return True


    def __setCardinal(self, faceIdx):
        xref, yref, zref = self.v[self.f[faceIdx][0][0]]
        same = [True, True, True]
        for i in self.f[faceIdx][1:]:
            x, y, z = self.v[i[0]]
            if same[0] and x != xref:
                same[0] = False
            if same[1] and y != yref:
                same[1] = False
            if same[2] and z != zref:
                same[2] = False

        if same[0]:
            if xref == self.xMin:
                self.west = faceIdx
            elif xref == self.xMax:
                self.east = faceIdx
        elif same[1]:
            if yref == self.yMin:
                self.down = faceIdx
            elif yref == self.yMax:
                self.up = faceIdx
        elif same[2]:
            if zref == self.zMin:
                self.north = faceIdx
            elif zref == self.zMax:
                self.south = faceIdx
        else:
            return False
        return True


    def getSquare(self, coords, exact = False):
        exact = exact and len(coords == 4)
        if len(coords) in [1, 2, 3, 4]:
            xs = []
            ys = []
            for x, y in coords:
                if not x in xs:
                    xs.append(x)
                if not y in ys:
                    ys.append(y)
            if exact and (len(xs) != 2 or len(ys) != 2):
                return None # not an exact square
            return [[min(xs), min(ys)], [max(xs), max(ys)]]
        return None


    def __checkDims(self):
        return self.xLen <= MC_CUBE_UNIT * 3 and self.yLen <= MC_CUBE_UNIT * 3 and self.zLen <= MC_CUBE_UNIT * 3


    def validate(self):
        self.__checkDims()
        if len(self.v) > 8: return False
        if len(self.f) > 6: return False

        for ii, ivects in enumerate(self.f):
            # set cardinal values
            status = self.__setCardinal(ii)
            if not status:
                return False
        return True



class Material:

    def __init__(self, name = "missing"):
        self.name = name
        self.texture = None






def parseO(obj, line):
    return obj.addCube(line[1].strip())

def parseV(obj, line):
    cube = obj.lastCube()
    cube.addV([float(line[1]), float(line[2]), float(line[3])])
    return

def parseVT(obj, line):
    cube = obj.lastCube()
    cube.addVT([float(line[1]), float(line[2])])
    return


def parseF(obj, line):
    cube = obj.lastCube()
    F = []
    for idx in line[1:]:
        v, vt, vn = idx.split("/")
        if vt != '':
            F.append([int(v), int(vt)])
        else:
            # no VTs
            F.append([int(v), 0])
    cube.addF(F)
    return

def parseUsemtl(obj, line):
    cube = obj.lastCube()
    cube.currentMtlIdx = obj.getMtlIdx(line[1].strip())


def parseNewmtl(obj, line):
    obj.addMtl(line[1].strip())


def parseMap_Kd(obj, line):
    mtl = obj.lastMtl()


TYPES_PARSE = {
    "o": parseO,
    "v": parseV,
    "f": parseF,
    "vt": parseVT,
    "usemtl" : parseUsemtl,
    "newmtl": parseNewmtl,
    "map_Kd": parseMap_Kd
}




def parseLine(obj, line):
    type = line[0]

    lineParser = TYPES_PARSE.get(type)
    if not lineParser:
        return

    return lineParser(obj, line)





def parseOBJ(filename, mtlFilename):

    obj = Obj()
    # parse mtl
    with open(mtlFilename, 'r') as f:
        line = f.readline().split(" ")

        while line != ['']:
            parseLine(obj, line)
            line = f.readline().split(" ")

    with open(filename, 'r') as f:

        line = f.readline().split(" ")

        while line != ['']:
            parseLine(obj, line)
            line = f.readline().split(" ")

    valid = obj.validate()
    if not valid:
        raise Exception("Object not valid.")


    return obj