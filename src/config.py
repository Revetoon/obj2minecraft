# Textures & model
DEFAULT_BASEPATH = "block"
DEFAULT_PARENT = "cube_all"
DEFAULT_TEXTURE = "oak_planks"

# blender object config
OBJ_CUBE_UNIT = 2
OBJ_CUBE_OFFSET = 1

# do not modify
MC_CUBE_UNIT = 16
MC_CUBE_BOUND_MAX = 32
MC_CUBE_BOUND_MIN = -16

# Output
DEFAULT_COMPACT_JSON = False
